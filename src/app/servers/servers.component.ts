import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServers=false;
  status="No Servers were created!";
  serverName="";
  isCreated=false;
  servers=["Server 1","Server 2"];
  constructor() { 
    setTimeout(()=>{this.allowNewServers=true;},5000);
  }
  onCreateServer(){
    this.status= this.serverName+" Server Created!"
    this.isCreated=true;
    this.servers.push(this.serverName); 
  }
  ngOnInit() {
  }
  onUpdateServerName(event){
    this.serverName = event.target.value;
  }
}
